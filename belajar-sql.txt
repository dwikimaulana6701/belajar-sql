1. Buat Databases
create database myshop;

2. Membuat table didalam database
a. Buat table users:
create table Users(
    -> id int(20) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

b. Buat table categories:
create table categories(
    -> id int(20) primary key auto_increment,
    -> name varchar(255)
    -> );

c. Buat table items:
create table items(
    -> id int(20) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(12),
    -> stock int(10),
    -> category_id int(7),
    -> foreign key(category_id) references categories(id)
    -> );

3. Memasukkan data pada table
Table Users
insert into Users(name,email,password) values
    -> ("John Doe","John@doe.com","john123"),
	 ("John Doe","John@doe.com","john123");

Table categories
insert into categories(name) values
    -> ("gadget"),("cloth"),("men"),("women"),("blanded");

Table items
insert into items(name,description,price,stock,category_id) values
    -> ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),
	 ("Uniklooh","baju keren dari brand ternama",500000,50,2),
	 ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Mengambil data dari Database
a. Menampilkan data Users kecuali password
   select id,name,email from Users;

b. Mengambil data items
 - Menampilkan data harga diatas 1000000
   select * from items where price > 1000000;
 - Mengambil data berdasarkan nama serupa(like)
   select * from items where name like '%sang%';

c. Menampilkan data items join dengan kategori
   select items.name, items.description, items.price, items.stock, items.category_id, categories.name
   -> from items inner join categories on items.category_id = categories.id;

5. Mengubah data dari databases
   update items set price = 2500000 where id = 1;